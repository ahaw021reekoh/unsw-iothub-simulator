'use strict'

//before using run the following
// npm  install azure-iot-device-mqtt
// npm install azure-iot-device
//npm install azure-iothub


//variables - these will be data points uploaded to IoT Hub

var temp1 = Math.round(100 * Math.random())
var pressure = Math.round(400 * Math.random())
var humidity = Math.round(100 * Math.random())

//Protocols - if you wan tto use protocols other than MQTT when you need to look at installing addition IoT Hub LIbraries

var MQTTProtocol = require('azure-iot-device-mqtt').Mqtt;
// var HTTPProtocol = require('azure-iot-device-http').Http;
// var AQMPProtocol = require('azure-iot-device-amqp').Amqp;
// var Protocol = require('azure-iot-device-mqtt').MqttWs;
// var AQMPProtocol = require('azure-iot-device-amqp').AmqpWs;

var Client = require('azure-iot-device').Client;
var Message = require('azure-iot-device').Message;

//get the connection string from azure portal IoT Hub

var connectionString = ""

var commsObject = Client.fromConnectionString(connectionString, MQTTProtocol);

var sendInterval = setInterval(function () {
  var windSpeed = 10 + (Math.random() * 4); // range: [10, 14]
  var temperature = 10 + (Math.random() * 30); // range: [20, 30]
  var humidity = 60 + (Math.random() * 20); // range: [60, 80]
  //var data = JSON.stringify({windSpeed: windSpeed, temperature: temperature, humidity: humidity });
  var data = `Temp: ${temperature}  Wind: ${windSpeed}  Humidity: ${humidity}`
  var message = new Message(data);
  console.log('Sending message: ' + message.getData());
  //message.properties.add('temperatureAlert', (temperature > 30) ? 'true' : 'false');
  commsObject.sendEvent(message);
}, 10000); // change 10000 to what you want for example 30000 will be 30 seconds
